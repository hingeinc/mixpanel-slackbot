import os
import configparser
import json
import boto3, botocore
from config import bucket


def get_user_coaches(users):
    print(str(len(users)) + " new matches identified")

    if all(x.isdigit() for x in users):
        try:
            with open('user_coaches.json', 'wb+') as data:
                bucket.download_fileobj('user_coaches.json', data)
            user_coaches = json.load(open('user_coaches.json'))
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("user_coaches.json does not exist.")
            else:
                raise

        try:
            output_user_coach = { user_id: user_coaches['coach.first_name'][str(user_id)] for user_id in users }
            return output_user_coach
        except Exception, e:
            print('User ID ' + str(e) + ' could not be matched to coach')
            return e

    else:
        print('User id not returned')
        return None
