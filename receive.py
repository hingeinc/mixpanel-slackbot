import os
from flask import Flask, request, Response
import json, ast
from app import send_message


app = Flask(__name__)

SLACK_WEBHOOK_SECRET = os.environ.get('SLACK_WEBHOOK_SECRET')


@app.route('/slack', methods=['POST'])
def inbound():
    if request.form.get('token') == SLACK_WEBHOOK_SECRET:
        channel = request.form.get('channel_name')
        username = request.form.get('user_name')
        text = request.form.get('text')
        inbound_message = username + " in " + channel + " says: " + text
        print(inbound_message)
    return Response(), 200


# @app.route('/', methods=['GET'])
# def test():
#     return Response('It works!')

@app.route('/reading_lag',methods=['POST'])
def route_data():
    r = ast.literal_eval(request.form.get('users'))
    users = []
    try:
        for item in r:
          users.append(item['$distinct_id'])
    except:
        pass
    outgoing_message = '@simon ' + ', '.join(users) + " currently suffering reading lags"
    send_message('C1RLS5JC9', outgoing_message)
    return "OK"


if __name__ == "__main__":
    app.run(debug=True)
