import os
import configparser
import datetime
from datetime import timedelta
from Mixpanel import Mixpanel
from tinydb import TinyDB, Query
from tinydb.operations import add
from app import app

class Observation(Mixpanel):

    db = TinyDB('observations_db.json')

    def __init__(self, notification_content):
        self.name = notification_content['name']
        self.event = notification_content['event']
        self.threshold = int(notification_content['threshold'])
        self.timeframe = int(notification_content['timeframe'])
        filters = notification_content['event_filters'] if 'event_filters' in notification_content else []
        self.event_filters = [{"property": filters[k]['property'], "argument": filters[k]['argument']} for k in filters]
        self.matches = None

    def get_matches(self):
        api = Mixpanel(api_secret=app.config['API_SECRET'])
        today = datetime.date.today()
        data = api.request(['segmentation'], {
            'event': self.event,
            'from_date': today - timedelta(days=self.timeframe),
            'to_date': today,
            'on': 'user["User ID"]',
            'type': 'general',
            'where': ' and '.join(['(properties["' + item['property']  + '"] == "' + item['argument'] + '")' for item in self.event_filters])
        })

        # sum the number of instances over the specified time period and keep them if they're greater than the threshold
        matched_users = [{"user_id": k,"count": sum(v.values()),"timestamp": str(datetime.datetime.now())} for k, v in data['data']['values'].iteritems() if sum(v.values()) > self.threshold]
        self.matches = matched_users
        return matched_users

    def get_new_matches(self):
        all_matches = self.get_matches()
        observation = Query()
        observations = Observation.db.search(observation.type == self.name)

        if len(observations) > 0:
            # if there's anything already in the db retrieve it and calculate the delta
            previous_matches = Observation.db.search(observation.type == self.name)[0]['data']
            previous_matched_users = [item['user_id'] for item in previous_matches]
            new_matches = [d for d in all_matches if d['user_id'] not in previous_matched_users]
        else:
            new_matches = all_matches

        return new_matches

    def purge_old_matches(self):
        """ Remove any observations that happened over a day ago """
        observation = Query()

        if Observation.db.contains(observation.type == self.name):
            # Get existing dict
            current_entries = Observation.db.search(observation.type == self.name)[0]['data']
            todays_entries = [entry for entry in current_entries if datetime.datetime.strptime(entry['timestamp'],"%Y-%m-%d %H:%M:%S.%f") > (datetime.datetime.now() - timedelta(days = 1))]
            Observation.db.update({'data': todays_entries}, observation.type == self.name)

        return


    def commit_matches(self, matches):
        observation = Query()

        if Observation.db.contains(observation.type == self.name):
            # Get current db entry and update it with the new matches
            updated_list = Observation.db.search(observation.type == self.name)[0]['data']
            updated_list.extend(matches)

            # Update the observation data
            Observation.db.update({'data': updated_list}, observation.type == self.name)
            print("New data added to the db")
        else:
            # Create the observation in the db
            Observation.db.insert({'type': self.name, 'data': matches})
            print("No observation matched, new entry created")

        return

    @classmethod
    def print_db(cls):
        for i in cls.db:
            print(i)
        return
