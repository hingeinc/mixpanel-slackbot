import os
import configparser
import boto3, botocore
import json

if os.environ.get('HEROKU') is not None:
    AWS_ACCESS_KEY_ID=os.environ.get('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY_ID=os.environ.get('AWS_SECRET_ACCESS_KEY_ID')
    API_SECRET=os.environ.get('MIXPANEL_API_SECRET')
    SHEET_ID=os.environ.get('SHEET_ID')
    KEYFILE = json.loads(os.environ.get('GOOGLE_KEYFILE'))
    SLACK_TOKEN = os.environ.get('SLACK_TOKEN')
    MIXPANEL_SLACK_CHANNEL = os.environ.get('MIXPANEL_SLACK_CHANNEL')
    COACH_SLACK_CHANNEL = os.environ.get('COACH_SLACK_CHANNEL')
elif os.path.exists("keys/keys.cnf"):
    config = configparser.ConfigParser()
    config.read("keys/keys.cnf")
    AWS_ACCESS_KEY_ID = config.get('AWS', 'AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY_ID = config.get('AWS', 'AWS_SECRET_ACCESS_KEY_ID')
    API_SECRET = config.get('MIXPANEL', 'API_SECRET')
    SHEET_ID = config.get('GOOGLE', 'SHEET_ID')
    KEYFILE = json.loads(config.get('GOOGLE', 'KEYFILE'))
    SLACK_TOKEN = config.get('SLACK', 'SLACK_BOT_TOKEN')
    MIXPANEL_SLACK_CHANNEL = config.get('SLACK', 'MIXPANEL_CHANNEL')
    COACH_SLACK_CHANNEL = config.get('SLACK', 'COACH_CHANNEL')

BUCKET_NAME = 'mixpanel.bot.database'

s3 = boto3.resource('s3',
aws_access_key_id=AWS_ACCESS_KEY_ID,
aws_secret_access_key=AWS_SECRET_ACCESS_KEY_ID)

bucket = s3.Bucket(BUCKET_NAME)
