import os
import configparser
from gspread import Client
import json
from oauth2client.service_account import ServiceAccountCredentials
from app import app

class GSheetClient(Client):

    scope = ['https://spreadsheets.google.com/feeds']
    credentials = ServiceAccountCredentials.from_json_keyfile_dict(app.config['KEYFILE'], scope)
    c = Client(auth=credentials)
    c.login()

    def __init__(self, sheet_id):
        self.sheet_id = sheet_id
        self.sheet = self.open_sheet()
        super(Client, self).__init__()

    def open_sheet(self):
        sheet = GSheetClient.c.open_by_key(self.sheet_id).sheet1
        return sheet

    def first_empty_row(self):
        all = self.sheet.get_all_values()
        row_num = 1
        consecutive = 0
        for row in all:
            flag = False
            for col in row:
                if col != "":
                    # something is there!
                    flag = True
                    break

            if flag:
                consecutive = 0
            else:
                # empty row
                consecutive += 1

            if consecutive == 2:
                # two consecutive empty rows
                return row_num - 1
            row_num += 1
        # every row filled
        return row_num

    def add_row(self, args):
        self.sheet.resize(self.first_empty_row()-1)
        self.sheet.append_row(args)
        return 'OK'
