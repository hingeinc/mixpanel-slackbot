import os
import configparser
from slackclient import SlackClient
from flask import Flask, request, Response
import json, ast

if os.environ.get('HEROKU') is not None:
    SLACK_TOKEN=os.environ.get('SLACK_TOKEN')
elif os.path.exists("keys/keys.cnf"):
    config = configparser.ConfigParser()
    config.read("keys/keys.cnf")
    SLACK_TOKEN = config.get('SLACK', 'TOKEN')

mixpanel_event_slack_channel = 'C31LHLXUM'

SLACK_WEBHOOK_SECRET = os.environ.get('SLACK_WEBHOOK_SECRET')

slack_client = SlackClient(SLACK_TOKEN)

def list_users():
    api_call = slack_client.api_call("users.list")
    if api_call.get('ok'):
        # retrieve all users so we can find our bot
        users = api_call.get('members')
        for user in users:
            print(user['name'] + "' is " + user.get('id'))
    else:
        print("Unable to authenticate.")

def list_channels():
    channels_call = slack_client.api_call("channels.list")
    if channels_call.get('ok'):
        return channels_call['channels']
    return None

def print_channels():
    channels = list_channels()
    if channels:
        print("Channels: ")
        for c in channels:
            print(c['name'] + " (" + c['id'] + ")")
    else:
        print("Unable to authenticate.")
