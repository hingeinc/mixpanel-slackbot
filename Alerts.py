import os
import configparser
from slackclient import SlackClient
from flask import Flask, request, Response
from helper_functions import get_user_coaches
from gsheets import GSheetClient
from Observations import Observation
from app import app
import gsheets
import json, ast
import boto3, botocore
import datetime


class Alert(SlackClient, GSheetClient):

    slack_client = SlackClient(app.config['SLACK_TOKEN'])
    gsheet = GSheetClient(app.config['SHEET_ID'])

    def __init__(self, users = None, notification_content = None, channel='mixpanel'):
        self.users = users
        self.simon_id = 'U02T5NAGU'
        self.observation_name = notification_content['name'] if notification_content is not None else None
        self.message_snippet = notification_content['message_snippet'] if notification_content is not None else None
        channel = notification_content['channel'] if notification_content is not None else channel
        if channel == 'mixpanel':
            self.channel = app.config['MIXPANEL_SLACK_CHANNEL']
        elif channel == 'coach':
            self.channel = app.config['COACH_SLACK_CHANNEL']
        else:
            print('No valid Slack channel specified')
            return


    def get_user_coaches(self):
        try:
            user_coaches = get_user_coaches(self.users)
            return user_coaches
        except Exception, e:
            self.send_ephemeral(message = str(e) + " could not match users to a coach")
            return

    def create_message_from_snippet(self, user_coaches):
        outgoing_message = ''
        if user_coaches and self.message_snippet is not None:
            for user, coach in user_coaches.items():
                outgoing_message = outgoing_message + '@'+ coach.lower() + ', <https://app.hingehealth.com/admin/users/' + user + '|user ' + user + '>' + self.message_snippet + '[<https://mixpanel.com/report/612577/explore/#user?distinct_id=' + user + '|Mixpanel>]\n'
        return outgoing_message

    def send_alert(self, message):
        call = Alert.slack_client.api_call(
            "chat.postMessage",
            as_user=True,
            channel=self.channel,
            text=message,
            link_names=True,
            unfurl_links=False
        )
        if not call['ok']:
            self.send_ephemeral(message = 'error in Alert.send_alert: ' + call['error'])
        return "ok"

    def send_ephemeral(self, message, notify=True):
        Alert.slack_client.api_call(
            "chat.postEphemeral",
            user=self.simon_id,
            as_user=True,
            channel=self.channel,
            text='@simon ' + message if notify else message,
            link_names=True
        )

    def create_and_send(self):
        user_coaches = self.get_user_coaches()
        message = self.create_message_from_snippet(user_coaches)
        self.send_alert(message=message)
        for user, coach in user_coaches.items():
            args = [user, coach, self.observation_name, str(datetime.datetime.now())]
            Alert.gsheet.add_row(args)



if __name__ == "__main__":
    app.run(debug=True)
