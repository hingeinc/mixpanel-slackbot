import os
from flask import Flask
from flask_script import Manager
from slackclient import SlackClient
import configparser
import datetime
from tinydb import TinyDB, Query
import boto3, botocore
from pandas import DataFrame

from app import app
from gsheets import GSheetClient
from Observations import Observation
from Alerts import Alert
import db_utils
from config import bucket
import time

manager = Manager(app)


def load_database():
    """ Grab the notifications and observations databases from S3 """

    print("Downloading db from S3")

    try:
        with open('notifications_db.json', 'wb+') as data:
            bucket.download_fileobj('notifications_db.json', data)
        with open('observations_db.json', 'wb+') as data:
            bucket.download_fileobj('observations_db.json', data)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("Observation or notification json does not exist on S3")
        else:
            raise


@manager.command
def add_notification():
    """ CLI for adding a new notification to the notifications database

    Function adds notification to the local TinyDB instance of notifications,
    then updates the notifications_db json on S3
    """
    notifications_db = TinyDB('notifications_db.json')

    name = raw_input("Please give your notification a name: ")
    event = raw_input("The Mixpanel event you're tracking: ")
    timeframe = raw_input("The time window in days: ")
    threshold = raw_input("The number of events above which the notification is triggered: ")
    message_snippet = raw_input("A message for the Slack notification: ")

    notifications_db.insert({
    'name': name,
    'event': event,
    'threshold': threshold,
    'timeframe':timeframe,
    'message_snippet': message_snippet
    })

    # Update the observations_db file on S3
    try:
        with open('notifications_db.json', 'wb') as data:
            bucket.upload_fileobj(data, 'notifications_db.json')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            alert.send_ephemeral(message = "Upload of notifications_db failed, The object does not exist.")
        else:
            raise

@manager.command
def generate_all_alerts():
    """ Main function used to generate all Slackbot alerts

    1. Pulls json data re notifications and today's observations from S3
    2. Creates Slack alerts for each notification type
    3. Updates the observation json on S3 with new matches
    """
    notifications_db = TinyDB('notifications_db.json')
    exception_alert = Alert()

    # Create Slack alerts where there are new observations & update observations_db
    for notification in notifications_db:
        try:
            create_alerts_for_notification(notifications_db, notification['name'])
        except Exception, e:
            message = 'Check failed on ' + notification['name'] + ': ' + str(e)
            exception_alert.send_ephemeral(message = message)
            print(message)
            continue
    # Update the observations_db json file on S3 with the new observations
    try:
        with open('observations_db.json', 'rb+') as data:
            bucket.upload_fileobj(data, 'observations_db.json')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            alert.send_ephemeral(message = "Upload of observations_db failed, The object does not exist.")
        else:
            raise

@manager.option('-n', '--name', dest='name', default='connection_drops')
def create_alerts_for_notification(db, name):
    """ Create Slack alerts for a given notification

    1. Create an observation object for the given notification
    2. Remove expired observations of this notification type from the db
    3. Hit the Mixpanel api for new matches to the observation
    4. Send alerts for any new matches
    5. Update the observation db with the new matches

    Input
        name       the name of the notification to generate alerts for
    """
    Notification = Query()
    notifications_db = db

    if notifications_db.contains(Notification.name == name):
        notification_content = dict(notifications_db.search(Notification.name == name)[0])
        print("Looking at " + notification_content['name'])

        # Purge expired observatinons and identify new matches
        observation = Observation(notification_content)
        observation.purge_old_matches()
        new_matches = observation.get_new_matches()

        # If any new matches send alerts and update the db
        if len(new_matches):
            users = [item['user_id'] for item in new_matches]
            alert = Alert(users = users, notification_content = notification_content)
            alert.create_and_send()

            # Update db
            observation.commit_matches(new_matches)
            # observation.print_db()
        else:
            print("No new matches")
    else:
        alert = Alert()
        alert.send_ephemeral(message = 'Error: tried to create a notification that doesn\'t exist')

    return "OK"

@manager.command
def purge_all_observations():
    open('observations_db.json', 'w').close()
    try:
        bucket.upload_file('observations_db.json', 'observations_db.json')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            alert.send_ephemeral(message = "Upload of observations_db.json failed, the file does not exist.")
        else:
            raise
    return "db burned to the ground"

@manager.command
def update_users():
    users = db_utils.get_data_api('user')
    teams = db_utils.get_data_api('team')


    user_teams = users[['id', 'team_id']]
    coach_names = teams[['id', 'coach.first_name']]

    user_coaches = user_teams.merge(coach_names, left_on='team_id', right_on='id')[['id_x', 'coach.first_name']]
    user_coaches.rename(columns={'id_x':'user_id'}, inplace=True)

    # Update the user_coaches file on S3
    try:
        with open('user_coaches.json', 'wb+') as data:
            data.write(user_coaches.set_index('user_id').to_json())
        bucket.upload_file('user_coaches.json', 'user_coaches.json')
        print('user_coaches.json updated')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            alert.send_ephemeral(message = "Upload of user_coaches failed, The object does not exist.")
        else:
            raise

@manager.command
def check_res_band_shipments():
    # read unchecked rows, return row with updated shipment details
    gsheet = GSheetClient(app.config['SHEET_ID']).sheet

    all_values = gsheet.get_all_values()
    df = DataFrame(all_values, columns=all_values.pop(0))
    new_shipments = df[(df["Resistance band ordered"]=='x') & (df["Coach notified"]=='')]
    print(str(len(new_shipments.index)) + ' new shipments identified')

    for index, row in new_shipments.iterrows():
        # Send coach alert
        alert = Alert(channel = 'coach')
        alert.send_alert('@' + row['Coach'].lower() + ', user ' + row['User ID'] + ' has just been shipped a resistance band, send them <https://docs.google.com/document/d/1mAGvcqYlaVCySepnPoMKzOBDYg-0U4iR5tbRq_4r7iY/edit#heading=h.yiej8p4xmq85 |this message> :package:')
        # Add acknowledgement
        acknowledge_column = gsheet.find("Coach notified").col
        gsheet.update_cell(index + 2, acknowledge_column, str(datetime.datetime.now()))
    return

if __name__ == "__main__":
    load_database()
    manager.run()
