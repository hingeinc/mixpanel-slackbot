# import packages
import configparser
import pandas as pd
import requests
import json
import os

def authenticate_api():
    """ Authenticate on Hinge Health API and return token headers
    :return: token_headers dict that needs to be attached to any API requests
    """

    if os.environ.get('HEROKU') is not None:
        email=os.environ.get('email')
        pwd=os.environ.get('password')
    elif os.path.exists("keys/keys.cnf"):
        config = configparser.ConfigParser()
        config.read("keys/keys.cnf")
        email = config.get('API KEY', 'email')
        pwd = config.get('API KEY', 'password')
    else:
        email = input("Email: ")
        pwd = input("Password: ")

    # establish connection
    endpoint_auth = "https://app.hingehealth.com/api/analysis/auth/sign_in"
    credentials = {'email': email, 'password': pwd}

    request_auth = requests.post(url=endpoint_auth, data=credentials, verify=True)
    assert request_auth.status_code == 200, "authentication failed"

    token_headers = {
        "access-token": request_auth.headers["access-token"],
        "token-type": "Bearer",
        "client": request_auth.headers["client"],
        "uid": request_auth.headers["uid"]
    }

    # validate token
    endpoint_validation = "https://app.hingehealth.com/api/analysis/auth/validate_token"
    validated = requests.get(url=endpoint_validation, data=token_headers, verify=True)
    assert validated.status_code == 200, "token not accepted"
    return token_headers


def get_data_api(model, flatten=True):
    """ Pull data over the analysis API.
    Add your HH password to a file called keys/keys.cnf

    Inputs
        :param model: string with name of a model
        :param flatten: boolean on whether to flatten the json object returned by server

    Returns
        A Pandas dataframe with data from the requested pages
    """

    endpoint_url = "https://app.hingehealth.com/api/analysis/resources/%s?items_per_page=500" % model

    request_data = json.loads(requests.get(
        endpoint_url,
        data=token_headers,
        verify=True).content.decode('utf-8'))
    # print(request_data)
    total_page_count = request_data['total_page_count']

    # Loop over data and collect the data into df
    page = 0
    finished = False
    while page < total_page_count:
        endpoint_data = "https://app.hingehealth.com/api/analysis/resources/%s?page=%d&items_per_page=500" % (model, page)
        df_temp = hh_api_call(endpoint_data, flatten=flatten)

        # add the df with this page data to the existing df, if it exists
        if page == 0:
            df = df_temp
        else:
            df = df.append(df_temp)

        # increment page count
        page += 1

    return df


def get_user_data_api(user_id, model='health_reports', flatten=True):
    """ Pull user-scoped data over the analysis API.
    Add your HH password to a file called keys/keys.cnf

    Inputs
        user_id     Integer indicating the user id
        model       The model to retrieve, scoped on user_id
        flatten     boolean on whether to flatten the json object returned by server

    Returns:
        df_temp     Pandas dataframe with 1 row (for the user) and columns depending on whether flatten is True or False
    """

    # Note that the endpoint is resources/user/, NOT resources/userS/. The latter can only return user records.
    endpoint_data = "https://app.hingehealth.com/api/analysis/resources/user/%d/%s" % (user_id, model)
    return hh_api_call(endpoint_data, flatten=flatten)


def hh_api_call(endpoint, flatten=True):
    """Calls the HH API and returns a df
    Input
        endpoint    URL with API endpoint
        flatten     whether or not to flatten the data

    Return
        False if the API call failed; otherwise Pandas df
    """
    print('Hitting up ' + endpoint)

    request = requests.get(
        endpoint,
        data=token_headers,
        verify=True)

    # check if API call was successful
    if request.status_code != 200:
        print('HH server returned code ' + str(request.status_code) + ' on endpoint ' + endpoint)
        return request.status_code

    # extract the data
    request_data = json.loads(request.content.decode('utf-8'))

    # # for debugging, print content
    # print(request_data)

    # sometimes the API contains pagination info, sometimes it doesnt. We only want the data itself
    if ('data' in request_data) and ('total_page_count' in request_data):  # this means its a paginated call
        # take the data out to prepare it for flattening
        request_data = request_data['data']

    if flatten:
        df_temp = flatten_into_df(request_data)
    else:
        df_temp = pd.DataFrame(request_data)

    return df_temp


def flatten_into_df(data_to_flatten):
    """ Takes data from the HH API and returns a flattened Pandas dataframe.
    Note this function can take a single dict, which becomes a 1-row df, or a list
    of dictionaries, each dict becoming one row.
    If you made a request to HH API that involved pagination, make sure to extract the
    data before passing the whole object to this function.

    Input
        data_to_flatten        a dictionary or list containing the data to end up in the dataframe.
    """

    # check if data is a dictionary - if it is, make it into a list
    if isinstance(data_to_flatten, dict):
        data_to_flatten = [data_to_flatten]

    for idx, item in enumerate(data_to_flatten):
        df_element = pd.io.json.json_normalize(item)
        if idx == 0:
            df_temp = df_element
        else:
            df_temp = df_temp.append(df_element)

    return df_temp


# run this once so it doesn't have to be done for every new API call anew.
token_headers = authenticate_api()
